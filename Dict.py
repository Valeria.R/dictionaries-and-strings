from typing import Dict, TextIO, List


def dict_to_str(d: Dict[int, int]) -> str:
    """
    Return a str containing each key and value in d. Keys and
    values are separated by a space. Each key-value pair is separated by a
    comma.

    >>> dict_to_str({3: 4, 5: 6})
    '3 4, 5 6'
    """
    s = ''

    for key in d:
        s += str(key) + ' '
        s += str(d[key]) + ', '
        
    return s[:-2]

x = {3: 4, 5: 6}
y = {}

#print(dict_to_str(y))

def dict_to_str_sorted(d: Dict[int, int]) -> str:
    """
    Return a str containing each key and value in d. Keys and
    values are separated by a space. Each key-value pair is separated by a
    comma, and the pairs are sorted in ascending order by key.

    >>> dict_to_str_sorted({5: 6, 3: 8})
    '3 8, 5 6'
    """
    new = {}
    keys = []

    for key in d:
        keys.append(key)

    keys.sort()
    for item in keys:
        new[item] = d[item]

    return dict_to_str(new)
        
z = {5: 6, 3: 8, 6: 2, 2: 10, 9: 11, 7: 0}
print(dict_to_str_sorted(z))

name = 'test.txt'

file = open(name, 'r')

def file_to_dict(f: TextIO) -> Dict[float, int]:
    """
    Return a dict containing the number of times each rate change that
    occurs in the open file f.  The dictionary should contain exchange rate
    changes as keys and the number of occurrences of each change as values.

    Note that this function takes in an open file, so you do not have to
    open the file again in the function. You should open the file in the
    main block below, and then pass in the open file to this function.
    """
    d = {}
    for line in f:
        line = line.split()
        for item in line:
            x = float(item)
            if x in d:
                d[x] += 1
            else:
                d[x] = 1

    return d

#print(file_to_dict(file))
v = file_to_dict(file)

def count_data(d: Dict[float, int]) -> int:
    """
    Return the total number of exchange rates (including duplicates) in
    the dictionary d of exchange rates.
    Note: the input to this function is the output of file_to_dict
    """
    total = 0

    for key in d:
        total += d[key]

    return total

#print(count_data(v))


def most_common_rate_change(d: Dict[float, int]) -> List[float]:
    """
    Return the exchange rate changes that occur the most frequently in
    the dictionary d of exchange rates.
    Note: the input to this function is the output of file_to_dict
    """
    most = 0
    name = []

    for key in d:
        if d[key] >= most:
            most = d[key]

    for key in d:
        if d[key] == most:
            name.append(key)

    return name

#print(most_common_rate_change(v))
    






    
